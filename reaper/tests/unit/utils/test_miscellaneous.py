import mock
import collections
from nova import test
from nova.reaper.utils import miscellaneous as misc


class MiscTests(test.TestCase):

    def setUp(self):
        super(MiscTests, self).setUp()

    def test_flavor_to_resources(self):
        pass

    def test_is_flavor_preemptible(self):
        pass

    def test_resources_comparisons(self):
        
        res1 = {
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        }
        res2 = {
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 10
        }

        self.assertTrue(misc.Resources(res1) >= misc.Resources(res2))
        self.assertTrue(misc.Resources(res1) != misc.Resources(res2))
        # TODO: check this
        self.assertFalse(misc.Resources(res1) > misc.Resources(res2))
        self.assertFalse(misc.Resources(res1) <= misc.Resources(res2))
        self.assertFalse(misc.Resources(res1) == misc.Resources(res2))

    def test_resources_sum_same_rcs(self):
        
        res1 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        res2 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 10
        })
        expected = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 30
        })
        self.assertTrue(res1 + res2 == expected)

    def test_resources_sum_not_same_rcs(self):
        
        res1 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        res2 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
        })
        expected = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        self.assertTrue(res1 + res2 == expected)

    def test_resources_sum_same_rcs(self):
        
        res1 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        res2 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        expected = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 40
        })
        self.assertTrue(res1 + res2 == expected)

    def test_resources_sub_not_same_rcs(self):
        
        res1 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        res2 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 10
        })
        expected = misc.Resources({
            'VCPU': 0,
            'MEMORY_MB': 0,
            'DISK_GB': 10
        })
        self.assertTrue(res1 - res2 == expected)

    def test_resources_mult(self):
        
        mult = 3
        res1 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        expected = misc.Resources({
            'VCPU': 3,
            'MEMORY_MB': 1536,
            'DISK_GB': 60
        })
        self.assertTrue(res1 * mult == expected)

    def test_resources_div(self):
        
        res1 = misc.Resources({
            'VCPU': 3,
            'MEMORY_MB': 1536,
            'DISK_GB': 60
        })
        res2 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        expected = 3
        self.assertTrue(res1 / res2 == expected)

    def test_resources_div_least(self):
        
        res1 = misc.Resources({
            'VCPU': 100,
            'MEMORY_MB': 5120,
            'DISK_GB': 40
        })
        res2 = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 20
        })
        expected = 2
        self.assertTrue(res1 / res2 == expected)
