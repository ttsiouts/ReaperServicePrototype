import mock
import collections
from nova import test
from nova.reaper.drivers import strict_driver
from nova.reaper.utils import miscellaneous as misc


class StrictDriverTests(test.TestCase):

    def setUp(self):
        super(StrictDriverTests, self).setUp()
        fake_ctx = mock.Mock()
        self.driver = strict_driver.StrictDriver(fake_ctx)

    def test_find_matching_combos(self):
        hosts = {}
        for i in range(0, 3):
            hosts.update({
                i: mock.Mock()
            })
        i = 0
        offer_res = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        for _, host in hosts.items():

            offer = misc.Offer('uuid_%s' % str(i),
                               ['consumer_%s' % str(i)],
                               offer_res * i)
            host.get_server_combinations.return_value = [offer]
            i += 1

        request = misc.Resources()
        offer = self.driver.find_matching_server_combinations(hosts, request)

        self.assertTrue(offer.provider == 'uuid_0')
        self.assertTrue(offer.consumers == ['consumer_0'])
        self.assertTrue(offer.resources == misc.Resources())

    def test_find_matching_combos(self):
        hosts = {}
        for i in range(0, 3):
            hosts.update({
                i: mock.Mock()
            })
        i = 0
        offer_res = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        for _, host in hosts.items():

            offer = misc.Offer('uuid_%s' % str(i),
                               ['consumer_%s' % str(i)],
                               offer_res * i)
            host.get_server_combinations.return_value = [offer]
            i += 1

        request = misc.Resources()
        offer = self.driver.find_matching_server_combinations(hosts, request)

        self.assertTrue(offer.provider == 'uuid_0')
        self.assertTrue(offer.consumers == ['consumer_0'])
        self.assertTrue(offer.resources == misc.Resources())

    def test_find_matching_combos_no_offers(self):
        hosts = {}
        for i in range(0, 3):
            hosts.update({
                i: mock.Mock()
            })
        for _, host in hosts.items():

            host.get_server_combinations.return_value = []

        request = misc.Resources()
        offer = self.driver.find_matching_server_combinations(hosts, request)

        self.assertTrue(offer == None)
