import mock
import collections
from nova import test
from nova.reaper.drivers import chance_driver
from nova.reaper.utils import miscellaneous as misc

class ChanceDriverTests(test.TestCase):

    def setUp(self):
        super(ChanceDriverTests, self).setUp()
        fake_ctx = mock.Mock()
        self.driver = chance_driver.ChanceDriver(fake_ctx)

    def test_select_servers(self):
        requested = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        host = mock.Mock()
        host.available = misc.Resources()
        consumer_res = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        offer = misc.Offer('rp_uuid', ['consumer1'], consumer_res)
        host.get_random_offer.return_value = offer
        selected = self.driver.select_servers(host, requested)

        self.assertTrue(selected == ['consumer1'])

    def test_select_more_offers(self):
        requested = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        host = mock.Mock()
        host.available = misc.Resources()
        consumer_res = misc.Resources({
            'VCPU': 1,
            'MEMORY_MB': 512,
            'DISK_GB': 10
        })
        offer1 = misc.Offer('rp_uuid', ['consumer1'], consumer_res)
        offer2 = misc.Offer('rp_uuid', ['consumer2'], consumer_res)

        host.get_random_offer.side_effect = [offer1, offer2]
        selected = self.driver.select_servers(host, requested)

        self.assertTrue(sorted(selected) == ['consumer1', 'consumer2'])

    def test_select_enough_available(self):
        requested = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        host = mock.Mock()
        host.available = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        selected = self.driver.select_servers(host, requested)

        self.assertTrue(selected == [])
        host.get_random_offer.assert_not_called()

    def test_select_no_offers(self):
        requested = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        host = mock.Mock()
        host.available = misc.Resources()
        consumer_res = misc.Resources()
        offer = misc.Offer('rp_uuid', [], consumer_res)

        host.get_random_offer.return_value = offer
        selected = self.driver.select_servers(host, requested)

        self.assertTrue(selected == [])

    def test_select_max_retries_exceeded(self):
        requested = misc.Resources({
            'VCPU': 2,
            'MEMORY_MB': 1024,
            'DISK_GB': 20
        })
        host = mock.Mock()
        host.available = misc.Resources()
        consumer_res = misc.Resources()
        offer = misc.Offer('rp_uuid', ['consumer'], consumer_res)

        host.get_random_offer.return_value = offer
        selected = self.driver.select_servers(host, requested)

        self.assertTrue(selected == [])
