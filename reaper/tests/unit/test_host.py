import mock
import collections
from nova import test
from nova.reaper.host import Host
from nova.reaper.utils import miscellaneous as misc

summary = {
   'VCPU': {
       'used': 2, 'capacity': 128
   },
   'MEMORY_MB': {
       'used': 1024, 'capacity': 21255
   },
   'DISK_GB': {
        'used': 70, 'capacity': 77
    }
}

preemptible_allocations = {
    'consumer1': {
        'VCPU': 1, 
        'MEMORY_MB': 512, 
        'DISK_GB': 50, 
        'CUSTOM_PREEMPTIBLE': 1
    }, 
    'consumer2': {
        'VCPU': 1, 
        'MEMORY_MB': 512, 
        'DISK_GB': 20, 
        'CUSTOM_PREEMPTIBLE': 1
    }
}


class HostTests(test.TestCase):

    def setUp(self):
        super(HostTests, self).setUp()
        self.host = Host('host_uuid', summary=summary)

    def test_populate_by_object(self):
        # TODO(ttsiouts): Check this initialization further
        fake = FakeCompute()
        expected_totals = misc.Resources({
           'VCPU': 128,
           'MEMORY_MB': 15512,
           'DISK_GB': 80
        })
        expected_available = misc.Resources({
           'VCPU': 125,
           'MEMORY_MB': 15000,
           'DISK_GB': 10
        })
        host = Host('uuid', host=fake)
        self.assertTrue(expected_totals == host.totals)
        self.assertTrue(expected_available == host.available)
        self.assertTrue(misc.Resources() == host.preemptible)
        self.assertTrue(misc.Resources() == host.reserved)

    def test_populate_by_summary(self):

        expected_totals = misc.Resources({
           'VCPU': 128,
           'MEMORY_MB': 21255,
           'DISK_GB': 77
        })
        expected_available = misc.Resources({
           'VCPU': 126,
           'MEMORY_MB': 20231,
           'DISK_GB': 7
        })
        host = Host('uuid', summary=summary)
        self.assertTrue(expected_totals == host.totals)
        self.assertTrue(expected_available == host.available)
        self.assertTrue(misc.Resources() == host.preemptible)
        self.assertTrue(misc.Resources() == host.reserved)

    def test_add_server_allocations(self):
        consumer1 = misc.Resources({
            'VCPU': 1, 
            'MEMORY_MB': 512, 
            'DISK_GB': 50, 
            'CUSTOM_PREEMPTIBLE': 1
        })
        consumer2 = misc.Resources({
            'VCPU': 1, 
            'MEMORY_MB': 512, 
            'DISK_GB': 20, 
            'CUSTOM_PREEMPTIBLE': 1
        })
        self.host.add_server_allocations(preemptible_allocations)
        self.assertTrue(self.host.preemptible == consumer1 + consumer2)
        self.assertTrue(
            self.host.servers.keys() == preemptible_allocations.keys()
        )

    def test_add_servers(self):
        pass

    def test_random_offer_out_of_index(self):
        expected = misc.Offer(self.host.uuid, [], misc.Resources())
        self.assertEqual(expected, self.host.get_random_offer(200))

    def test_validate_request(self):
        res = {
           'VCPU': 1,
           'MEMORY_MB': 512,
           'DISK_GB': 50
        }
        request = misc.Resources(res)
        self.host.available = misc.Resources(res)
        # Host's available resources are enough
        self.assertTrue(self.host.validate_request(request))
        self.host.available = misc.Resources()
        # Host has no available and no preemptible
        self.assertFalse(self.host.validate_request(request))
        preemptible = {
            'consumer1': {
                'VCPU': 1, 
                'MEMORY_MB': 512, 
                'DISK_GB': 50, 
                'CUSTOM_PREEMPTIBLE': 1
            }
        } 
        self.host.add_server_allocations(preemptible)
        # Host's preemptible resources are enough
        self.assertTrue(self.host.validate_request(request))

    def test_get_server_combinations(self):
        preemptible = {
            'consumer1': {
                'VCPU': 1, 
                'MEMORY_MB': 512, 
                'DISK_GB': 50, 
                'CUSTOM_PREEMPTIBLE': 1
            }
        } 
        request = misc.Resources({
           'VCPU': 1, 
           'MEMORY_MB': 512, 
           'DISK_GB': 50
        })
        self.host.add_server_allocations(preemptible)
        self.host.available = misc.Resources()
        offer = self.host.get_server_combinations(request)
        self.assertTrue(len(offer) == 1)
        offer = offer[0]
        self.assertEqual(self.host.uuid, offer.provider)
        self.assertEqual(['consumer1'], offer.consumers)
        self.assertTrue(request <= offer.resources)

    def test_get_server_combinations(self):
        preemptible = {
            'consumer1': {
                'VCPU': 1, 
                'MEMORY_MB': 512, 
                'DISK_GB': 50, 
                'CUSTOM_PREEMPTIBLE': 1
            }
        }
        request = misc.Resources({
           'VCPU': 2,
           'MEMORY_MB': 1024, 
           'DISK_GB': 60
        })
        self.host.add_server_allocations(preemptible)
        self.host.available = misc.Resources({
           'VCPU': 1,
           'MEMORY_MB': 512, 
           'DISK_GB': 15
        })
        offer = self.host.get_server_combinations(request)
        self.assertTrue(len(offer) == 1)
        offer = offer[0]
        self.assertEqual(self.host.uuid, offer.provider)
        self.assertEqual(['consumer1'], offer.consumers)
        self.assertTrue(request > offer.resources)

    def test_get_server_combinations_only_one_valid(self):
        request = misc.Resources({
           'VCPU': 2, 
           'MEMORY_MB': 1024, 
           'DISK_GB': 50
        })
        self.host.add_server_allocations(preemptible_allocations)
        self.host.available = misc.Resources()
        offer = self.host.get_server_combinations(request)
        self.assertTrue(len(offer) == 1)
        offer = offer[0]
        self.assertEqual(self.host.uuid, offer.provider)
        self.assertEqual(['consumer1', 'consumer2'], offer.consumers)
        res = self.host.servers['consumer1'] + self.host.servers['consumer2']
        self.assertTrue(res == offer.resources)

    def test_get_server_combinations_no_valid(self):
        request = misc.Resources({
           'VCPU': 1,
           'MEMORY_MB': 1024,
           'DISK_GB': 81
        })
        self.host.add_server_allocations(preemptible_allocations)
        self.host.available = misc.Resources()
        offer = self.host.get_server_combinations(request)
        self.assertTrue(len(offer) == 0)

    def test_get_server_combinations_enough_available(self):
        request = misc.Resources({
           'VCPU': 1,
           'MEMORY_MB': 1024,
           'DISK_GB': 5
        })
        offer = self.host.get_server_combinations(request)
        self.assertTrue(len(offer) == 1)
        offer = offer[0]
        self.assertEqual(self.host.uuid, offer.provider)
        self.assertEqual([], offer.consumers)
        self.assertTrue(misc.Resources() == offer.resources)

    def test_reserve_resources_less_than_requested(self):
        request = misc.Resources({
           'VCPU': 2, 
           'MEMORY_MB': 1024, 
           'DISK_GB': 50
        })

        self.host.add_server_allocations(preemptible_allocations)
        previous_available = self.host.available
        consumer1 = self.host.servers['consumer1']
        self.host.reserve_resources(['consumer1'], request)
        self.assertTrue('consumer1' not in self.host.servers.keys())
        self.assertTrue(
            self.host.servers['consumer2'] == self.host.preemptible
        )
        delta_available = previous_available - self.host.available
        delta_requested = request - consumer1
        self.assertTrue(delta_available == delta_requested)
        self.assertTrue(self.host.reserved == request)

    def test_reserve_resources_more_than_requested(self):
        request = misc.Resources({
           'VCPU': 1, 
           'MEMORY_MB': 512, 
           'DISK_GB': 50
        })

        self.host.add_server_allocations(preemptible_allocations)
        previous_available = self.host.available
        res = self.host.servers['consumer1'] + self.host.servers['consumer2']
        self.host.reserve_resources(['consumer1', 'consumer2'], request)
        self.assertTrue('consumer1' not in self.host.servers.keys())
        self.assertTrue('consumer2' not in self.host.servers.keys())
        self.assertTrue(self.host.reserved == request)
        self.assertTrue(self.host.preemptible == misc.Resources())
        # Since the reserved offer was more than the requested resources, we
        # end up with extra available space        
        self.assertTrue(
            previous_available < self.host.available
        )

    def test_provider_summary(self):
        pass

    def test_provider_allocations(self):
        pass


class FakeCompute(object):

    def __init__(self):
        self.vcpus = 8
        self.vcpus_used = 3
        self.cpu_allocation_ratio = 16.0
        self.memory_mb_used = 512
        self.free_ram_mb = 15000
        self.local_gb = 80
        self.local_gb_used = 70
        self.disk_allocation_ratio = 1.0
