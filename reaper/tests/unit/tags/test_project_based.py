import collections
import mock
from nova.reaper.tags import project_based
from nova import test


class ProjectTaggingTests(test.TestCase):

    def setUp(self):
        super(ProjectTaggingTests, self).setUp()
        mock_ctx = mock.Mock()
        self.tagging = project_based.ProjectTagging(mock_ctx)

    @mock.patch('nova.reaper.tags.project_based.Host') 
    def test_map_preemptibles_to_hosts(self, Host):
        projects = []
        for i in range(0, 3):
            project = mock.Mock()
            project.id = i
            projects.append(project)
        
        keystone_client = mock.Mock()
        keystone_client.get_preemptible_projects.return_value = projects
        self.tagging.keystone = keystone_client       

        servers = []
        for i in range(0, 3):
            server = mock.Mock()
            server.host = 'host'+str(i)
            servers.append([server])
        compute_client = mock.Mock()
        compute_client.get_project_instances.side_effect = servers

        compute_nodes = []
        uuids = []
        for i in range(0, 3):
            compute_node = mock.Mock()
            uuid = 'uuid'+str(i)
            compute_node.uuid = uuid
            compute_nodes.append(compute_node)
            uuids.append(uuid)

        compute_client.get_compute_node.side_effect = compute_nodes

        self.tagging.compute = compute_client
        hosts = self.tagging.map_preemptibles_to_hosts()
        self.assertTrue(list(hosts.keys()), uuids)
