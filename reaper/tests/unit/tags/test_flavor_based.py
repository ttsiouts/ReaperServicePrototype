import collections
import mock
from nova.reaper.tags import flavor_based
from nova import test


class FlavorTaggingTests(test.TestCase):

    def setUp(self):
        super(FlavorTaggingTests, self).setUp()
        mock_ctx = mock.Mock()
        self.tagging = flavor_based.FlavorTagging(mock_ctx)

    def test_is_flavor_preemptible(self):
        flavor = mock.Mock()
        flavor.extra_specs = dict()
        self.assertFalse(self.tagging.is_flavor_preemptible(flavor))
        flavor.extra_specs = {'preemptible' : False}
        self.assertFalse(self.tagging.is_flavor_preemptible(flavor))
        flavor.extra_specs = {'preemptible' : True}
        self.assertTrue(self.tagging.is_flavor_preemptible(flavor))

    @mock.patch('nova.reaper.tags.flavor_based.Host') 
    def test_map_preemptibles_to_hosts(self, Host):
        flavors = []
        for i in range(0, 3):
            flavor = mock.Mock()
            flavor.flavorid = i
            flavors.append(flavor)

        compute_client = mock.Mock()
        compute_client.get_preemptible_flavors.return_value = flavors

        servers = []
        for i in range(0, 3):
            server = mock.Mock()
            server.host = 'host'+str(i)
            servers.append([server])
        compute_client.get_instances_by_flavor.side_effect = servers

        compute_nodes = []
        uuids = []
        for i in range(0, 3):
            compute_node = mock.Mock()
            uuid = 'uuid'+str(i)
            compute_node.uuid = uuid
            compute_nodes.append(compute_node)
            uuids.append(uuid)

        compute_client.get_compute_node.side_effect = compute_nodes

        self.tagging.compute = compute_client
        hosts = self.tagging.map_preemptibles_to_hosts()
        self.assertTrue(list(hosts.keys()), uuids)
