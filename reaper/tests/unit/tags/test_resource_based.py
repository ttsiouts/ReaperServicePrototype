import collections
import mock
from nova.reaper.tags import resource_based
from nova import test


class ResourceTaggingTests(test.TestCase):

    def setUp(self):
        super(ResourceTaggingTests, self).setUp()
        mock_ctx = mock.Mock()
        self.tagging = resource_based.ResourceTagging(mock_ctx)

    @mock.patch('nova.reaper.tags.resource_based.Host') 
    def test_map_preemptibles_to_hosts(self, Host):
        flavors = []
        for i in range(0, 3):
            flavor = mock.Mock()
            flavor.flavorid = i
            flavors.append(flavor)
        
        compute_client = mock.Mock()
        compute_client.get_preemptible_flavors.return_value = flavors
       
        servers = []
        for i in range(0, 3):
            server = mock.Mock()
            server.host = 'host'+str(i)
            servers.append([server])
        compute_client.get_instances_by_flavor.side_effect = servers

        compute_nodes = []
        uuids = []
        for i in range(0, 3):
            compute_node = mock.Mock()
            uuid = 'uuid'+str(i)
            compute_node.uuid = uuid
            compute_nodes.append(compute_node)
            uuids.append(uuid)

        compute_client.get_compute_node.side_effect = compute_nodes

        self.tagging.compute = compute_client
        hosts = self.tagging.map_preemptibles_to_hosts()
        self.assertTrue(list(hosts.keys()), uuids)
