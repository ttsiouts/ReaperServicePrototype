from nova import compute
import nova.conf
from nova import exception
from nova.objects.flavor import FlavorList
from nova.reaper.utils.exceptions import catch_nova
from nova.reaper.utils import miscellaneous as misc
from oslo_log import log as logging
import time

LOG = logging.getLogger(__name__)
CONF = nova.conf.CONF


class ComputeClient(object):
    """Information from the Compute API

    This class is the interface for the reaper to place requests to the Nova
    API directly.
    """

    def __init__(self, context):
        self.compute_api = compute.API()
        self.context = context

    @catch_nova
    def get_project_instances(self, project_id):
        """Get a project's servers

        Returns the servers from the provided project
        """
        opts = {"tenant_id": project_id, "deleted": False}
        elevated = self.context.elevated()
        servers = self.compute_api.get_all(elevated, search_opts=opts)
        # NOTE(ttsiouts): Filter out the unassigned for now. This has to
        # be handled better... (if server.host)
        return [server for server in servers if server.host]

    @catch_nova
    def get_host_instances(self, host_id):
        """Get a project's servers

        Returns the servers from the provided host
        """
        opts = {"host": host_id, "deleted": False}
        elevated = self.context.elevated()
        return self.compute_api.get_all(elevated, search_opts=opts)

    def delete_instance(self, instance_id):
        """Deletes the given instance

        :param instance: the instance_id to be deleted
        """
        elevated = self.context.elevated()
        instance = self.compute_api.get(elevated,
                                        instance_id)
        self.compute_api.delete(elevated, instance)
        self._wait_until_deleted(instance_id)

    def get_instances_by_flavor(self, flavorid):
        """Finds instances based on the provided flavorid

        :param flavorid: the flavorid to match instances to 
        """
        elevated = self.context.elevated()
        opts = {
            "flavor": flavorid,
            "deleted": False
        }
        instance_list = self.compute_api.get_all(elevated,
                                            search_opts=opts,
                                            sort_keys=['created_at', 'host'],
                                            sort_dirs=['desc', 'asc'])
        return instance_list.objects

    @catch_nova
    def get_preemptible_flavors(self):
        
        e = self.context.elevated()
        return [f for f in FlavorList.get_all(e)
                if misc._is_flavor_preemptible(f)]

    def get_compute_node(self, host):
        e = self.context.elevated()
        res = compute.HostAPI().compute_node_search_by_hypervisor(e, host)
        return res[0]

    def _wait_until_deleted(self, instance_id, timeout=10):
        """Wait until the instance is deleted

        Tries to get the instance until it's not found or the timeout exceeds
        """
        # Awful way to wait until the instance is deleted. 
        # Have to live with it for now...
        # TODO(ttsiouts): Handle failure
        start = time.time()
        now = start
        while now - start <= timeout:
            try:
                self.compute_api.get(self.context.elevated(), instance_id)
            except exception.InstanceNotFound:
                break
            now = time.time()
