from keystoneauth1 import loading as keystone
from nova import exception as nova_exceptions
from nova.reaper.utils import exceptions
from nova.reaper.utils import miscellaneous as misc
from oslo_log import log as logging

import nova.conf

LOG = logging.getLogger(__name__)
CONF = nova.conf.CONF


class PlacementClient(object):
    """Allocations information from the Placement API.

    This class provides a the interface with the Placement API to calculate
    the resources allocated to already existing servers.
    """

    def __init__(self):
        auth_plugin = keystone.load_auth_from_conf_options(
            CONF, 'placement'
        )
        self.placement_client = keystone.load_session_from_conf_options(
            CONF, 'placement', auth=auth_plugin,
            additional_headers={'accept': 'application/json'}
        )
        self.ks_filter = {
            'service_type': 'placement',
            'region_name': CONF.placement.os_region_name,
            'interface': CONF.placement.os_interface
        }

    def get(self, url, version='1.14'):
        kwargs = {
            'headers': {
                'OpenStack-API-Version': 'placement %s' % version
            },
        }
        return self.placement_client.get(url,
            endpoint_filter=self.ks_filter, raise_exc=False, **kwargs)

    def get_resource_providers(self):
        """Returns all resource providers"""
        url = '/resource_providers'
        response = self.get(url)
        return response.json()

    def get_preemptible_allocations(self, consumers):
        allocations = []
        for consumer in consumers:
            allocations += self.get_allocations(consumer.uuid)
        return allocations

    def get_candidates_with_remaining(self, resources):
        query = "DISK_GB:%s,MEMORY_MB:%s,VCPU:%s" % \
                    (resources.disk, resources.memory, resources.vcpu)
        qs_params = 'resources=%s' % query

        url = "/allocation_candidates?%s" % qs_params
        resp = self.get(url, version='1.10')
        if resp.status_code == 200:
            data = resp.json()
            return data['allocation_requests'], data['provider_summaries']
        return {}, {}

    def get_culling_candidates(self, resources):
        """Returns culling candidates

        Transorms the requested resources and places a request to the
        Placement API. The response is in the form preemptible_allocations,
        provider_summaries.

        :param resources: an instance of the nova.reaper.utils.Resources
                          object, representing the amount of requested
                          resources
        """
        query = ','.join(["%s:%s" % (rc, amount)
            for rc, amount in resources.items()])
        qs_params = 'resources=%s' % query

        url = "/culling_candidates?%s" % qs_params

        resp = self.get(url, version='1.10')
        if resp.status_code == 200:
            data = resp.json()
            return data['preemptible_allocations'], data['provider_summaries']
        return None, None

    def get_allocations(self, consumer):
        """Returns allocations for the provided consumer

        :param consumer: the consumer id to get the allocations for
        """
        url = '/allocations/%s' % consumer
        response = self.get(url)
        if response.json()['allocations'] == {}:
            raise nova_exceptions.NotFound()
        json = response.json()
        return misc.json_to_allocations(json["allocations"], consumer)

    def get_provider_inventories(self, resource_provider):
        """Returns the inventories of a given provider

        :param resource_provider: the provider to search for
        """
        url = "resource_providers/%s/inventories" % resource_provider
        response = self.get(url)
        return response.json()["inventories"]

    def get_provider_usages(self, resource_provider):
        """Returns the usages of a given provider

        :param resource_provider: the provider to search for
        """
        url = "resource_providers/%s/usages" % resource_provider
        response = self.get(url)
        return response.json()['usages']

    def get_resource_class_by_name(self, name):
        url = 'resource_classes/%s' % name
        response = self.get(url)
        return response.json()
