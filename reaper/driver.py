import time

from nova.reaper.clients.computes import ComputeClient
from nova.reaper.clients.resources import PlacementClient
from nova.reaper.utils import exceptions

from oslo_log import log as logging
from oslo_serialization import jsonutils

LOG = logging.getLogger(__name__)


class ReaperDriver(object):
    """The base Reaper Driver class

    This is the class that all the Drivers for the Reaper Service have to
    inherit from
    """

    def __init__(self, context):
        self.compute = ComputeClient(context)
        self.placement = PlacementClient()
        self.context = context

    def get_preemptible_servers(self, requested, hosts, num_instances):
        # NOTE(ttsiouts): Every driver should override this method and
        # implement the strategy of the freeing
        raise NotImplementedError

    def cull_preemptible_servers(self, selected_instances):
        """Culls a preemptible server

        Through the Nova client, places a request to shut off and then delete
        a preemtpible server.
        """
        # NOTE(ttsiouts): Killing and waiting in different loops to save time
        for instance in selected_instances:
            # TODO(ttsiouts): terminate instead of deleting....
            self.compute.delete_instance(instance.uuid)

        for instance in selected_instances:
            self.wait_until_allocation_is_deleted(instance.uuid)

    def wait_until_allocation_is_deleted(self, instance_id, timeout=10):
        """Wait until the allocation is deleted

        Tries to get the allocations of a given instance until it's not found
        or the timeout exceeds
        """
        # Awful way to wait until the allocation is removed.....
        # Have to live with it for now... If we don't wait here, the claiming
        # of the resources will most probably fail since placement will not be
        # updated right away....
        start = time.time()
        now = start
        while now - start <= timeout:
            try:
                self.placement.get_allocations(instance_id)
            except exceptions.NotFound:
                break
            now = time.time()

    def form_scheduler_response(self, hosts, request):
        """Form the scheduler response

        This is where the response to the scheduler is being formed
        """
        # TODO(ttsiouts): Add handling for placement microversions.
        # The format of the response is changed...
        if not hosts:
            return [], {}

        allocations = list()
        provider_summaries = dict()

        # NOTE: For each host, form the response to the scheduler
        for host in hosts:
            allocations += host.get_provider_allocations(request)
            provider_summaries.update(host.get_provider_summary(request))

        return jsonutils.loads(jsonutils.dumps(allocations)), \
                   jsonutils.loads(jsonutils.dumps(provider_summaries))
