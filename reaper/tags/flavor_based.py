import collections
from nova.reaper.host import Host
from nova.reaper import tagging

from oslo_log import log as logging

LOG = logging.getLogger(__name__)


class FlavorTagging(tagging.Tagging):

    def __init__(self, context):
        super(FlavorTagging, self).__init__(context)

    def validate_request(self, req_spec):
        return not self.is_flavor_preemptible(req_spec.flavor)

    def is_flavor_preemptible(self, flavor):
        # NOTE(ttsiouts): For this stage the preemptible property is set to
        # the flavor. So here we check if the flavor has preemptible=True in
        # its extra_specs
        try:
            return flavor.extra_specs['preemptible']
        except KeyError:
            return False

    def get_relevant_hosts(self, request):
        return self.map_preemptibles_to_hosts()

    def map_preemptibles_to_hosts(self):
        """Maps preemptible instances to hosts.

        Gets the preemptible instances in the system and returns a map of
        preemptible instances per host.
        """
        host_servers = collections.defaultdict(list)
        hosts = {}
        flavors = self.compute.get_preemptible_flavors()
        for flavor in flavors:
            instances = self.compute.get_instances_by_flavor(flavor.flavorid)
            for server in instances:
                host_servers[server.host].append(server)

        for host, servers in host_servers.items():
            compute = self.compute.get_compute_node(host)
            hosts[compute.uuid] = Host(uuid=compute.uuid, host=compute)
            hosts[compute.uuid].add_servers(servers)

        return hosts
