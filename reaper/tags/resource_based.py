import collections

from nova.reaper import conf
from nova.reaper.host import Host
from nova.reaper import tagging
from nova.scheduler import utils as sched_utils

from oslo_log import log as logging

LOG = logging.getLogger(__name__)
CONF = conf.CONF

def _is_request_preemptible(resources):
    # NOTE(ttsiouts): For this stage the preemptible property is set to
    # the flavor. So here we check if the flavor has preemptible=True in
    # its extra_specs
    preemptible_resource = CONF.reaper.custom_resource
    requested_resources = resources.merged_resources()
    return preemptible_resource in requested_resources.keys()


class ResourceTagging(tagging.Tagging):

    def __init__(self, context):
        super(ResourceTagging, self).__init__(context)
        self.host_map = {}

    def get_relevant_hosts(self, request):
        return self.map_preemptibles_to_hosts(request)

    def validate_request(self, req_spec):
        resources = sched_utils.resources_from_request_spec(req_spec)
        return not _is_request_preemptible(resources)

    def map_preemptibles_to_hosts(self, request):
        """Maps preemptible instances to hosts.

        Gets the preemptible instances in the system and returns a map of
        instances per host.
        """
        res = request.to_dict()
        # Here we make use of a new route in Placement API. Instead of
        # requesting for allocation candidates, we request for culling
        # candidates. The query in the DB for each resource class is:
        # rc_used + rc_requested - rc_preemptible <= rc_total.
        candidates, summaries = self.placement.get_culling_candidates(res)

        hosts = collections.defaultdict()
        for uuid, summary in summaries.items():
            servers = candidates[uuid]
            # populate each host representation here, with the
            # info from Placement.
            host = Host(uuid, summary=summary['resources'])
            host.add_server_allocations(servers)
            hosts[uuid] = host

        return hosts
