import collections
from nova.reaper.clients.projects import KeystoneClient
from nova.reaper.host import Host
from nova.reaper import tagging

from oslo_log import log as logging

LOG = logging.getLogger(__name__)


class ProjectTagging(tagging.Tagging):

    def __init__(self, context):
       super(ProjectTagging, self).__init__(context)
       self.keystone = KeystoneClient()

    def validate_request(self, req_spec):
        return not self._is_project_preemptible(req_spec.project_id)

    def _is_project_preemptible(self, project_id):
        # NOTE(ttsiouts): As a first step here we have to check the project
        # where the server belongs to. If there the property "preemptible" 
        # exists and its value is True then the requested server is 
        # preemptible and this functionality is not supported.
        return self.keystone.is_project_preemptible(project_id)

    def get_relevant_hosts(self, request):
        return self.map_preemptibles_to_hosts()

    def map_preemptibles_to_hosts(self):
        """Maps preemptible instances to hosts.

        Gets the preemptible instances in the system and returns a map of
        instances per host.
        """
        host_servers = collections.defaultdict(list)
        hosts = {}
        # Get all the projects marked as preemptible
        projects = self.keystone.get_preemptible_projects()
        for project in projects:
            # For each preemptible project, fetch all the instances
            instances = self.compute.get_project_instances(project.id)
            for server in instances:
                host_servers[server.host].append(server)

        for host, servers in host_servers.items():
            compute = self.compute.get_compute_node(host)
            hosts[compute.uuid] = Host(uuid=compute.uuid, host=compute)
            hosts[compute.uuid].add_servers(servers)

        return hosts
