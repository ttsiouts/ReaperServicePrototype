from nova.reaper.clients.computes import ComputeClient
from nova.reaper.clients.resources import PlacementClient
from nova.reaper.clients.projects import KeystoneClient

from oslo_log import log as logging

LOG = logging.getLogger(__name__)


class Tagging(object):

     def __init__(self, context):
        self.compute = ComputeClient(context)
        self.keystone = KeystoneClient()
        self.placement = PlacementClient()
        self.context = context

     def get_relevant_hosts(self):
         pass

     def validate_request(self, request):
         pass
