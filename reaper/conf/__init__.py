from oslo_config import cfg

from nova.reaper.conf import reaper

CONF = cfg.CONF

reaper.register_opts(CONF)
