from oslo_config import cfg

reaper_group = cfg.OptGroup(name="reaper",
                            title="Reaper configuration")

reaper_opts = [
    cfg.StrOpt("driver",
        default="chance_driver",
        choices=("chance_driver", "strict_driver"),
        help="""
The driver that is used by the reaper.

The class of the driver used by the reaper. The name of the default driver is
'chance_driver'. This driver tries to make the reqested space by terminating
preemptible servers in a random order

Other options are:

* 'strict_driver': Tries to minimize the unused space left by calculating the
                   optimal combination of servers
"""),
    cfg.StrOpt("custom_resource",
        default="CUSTOM_PREEMPTIBLE",
        help="""
The custom preemptible resource class.

This is the way to distinguish between the non-preepmtible allocations and the
preemptible ones when the resource driver is used. Each preemptible server has
an allocation to this resource.
"""),
]

chance_driver_group = cfg.OptGroup(name="chance_driver",
                                   title="Chance Driver options")

chance_driver_opts = [
    cfg.IntOpt("max_attempts",
        default=3,
        help="""
The number of retries with a host.

This value controls the maximum attempts, the chance_driver will try, to find
the requested resources with each host, by randomly selecting servers to cull.
"""),
    cfg.IntOpt("alt_options",
        default=1,
        help="""
The number of alternative options for the scheduler.

This value controls the maximum slots that the chance_driver will try to free.
Each of the allocations, will be returned as a scheduling option.
"""),
]

strict_driver_group = cfg.OptGroup(name="strict_driver",
                                   title="Strict Driver options")

strict_driver_opts = [
    cfg.IntOpt("max_attempts",
        default=3,
        help="""
The number of retries with a host.

This value controls the maximum attempts, the strict_driver will try, to find
the requested resources with each host, by randomly selecting servers to cull.
"""),
    cfg.IntOpt("alt_options",
        default=1,
        help="""
The number of alternative options for the scheduler.

This value controls the maximum slots that the strict_driver will try to free.
Each of the allocations, will be returned as a scheduling option.
"""),
]


def register_opts(conf):
    conf.register_group(reaper_group)
    conf.register_opts(reaper_opts, group=reaper_group)

    conf.register_group(chance_driver_group)
    conf.register_opts(chance_driver_opts, group=chance_driver_group)

    conf.register_group(strict_driver_group)
    conf.register_opts(strict_driver_opts, group=strict_driver_group)
