import collections
from oslo_log import log as logging
import random

from nova.reaper import conf
from nova.reaper import driver
from nova.reaper.utils import miscellaneous as misc

LOG = logging.getLogger(__name__)
CONF = conf.CONF


class StrictDriver(driver.ReaperDriver):

    def __init__(self, context):
        super(StrictDriver, self).__init__(context)

    def get_preemptible_servers(self, requested, hosts, num_instances):
        """Implements the strategy of freeing up the requested resources.

        :param req_spec: an instance of the RequestSpec class representing the
                         requested resources
        :param resources: (Why is this here?)
        """
        selected = list()
        selected_hosts = list()

        # This is the maximum number of spots that we'll try to free up
        max_allocs = num_instances * CONF.strict_driver.alt_options

        for i in range(0, max_allocs):

            # Find all the matching flavor combinations and order them
            combo = self.find_matching_server_combinations(hosts, requested)

            if not combo:
                # If we run out of combos before the max retries break and
                # check if we have enough spots reserved.
                break

            host = hosts[combo.provider]

            # Reserve the resources to enable us to reuse the host
            host.reserve_resources(combo.consumers, requested)

            if host not in selected_hosts:
                selected_hosts.append(host)

            selected += combo.consumers

        # If we run out of combos before the max retries, we need to
        # check if we have enough spots reserved. If not, we won't
        # kill any servers. The least number of reserved spots is the
        # number of the requested instances.
        spots = 0
        for host in selected_hosts:
            spots += (host.reserved + host.available) / requested

        if spots < num_instances:
            selected = list()
            selected_hosts = list()

        return selected_hosts, selected

    def find_matching_server_combinations(self, hosts, requested):
        """Find the best matching combination

        The purpose of this feature is to eliminate the idle resources. So the
        best matching combination is the one that makes use of the most
        available space on a host.
        """
        # NOTE: Get the flavor combinations from the hosts
        combinations = []
        for _, host in hosts.items():
            combinations += host.get_server_combinations(requested)

        if not combinations:
            return None

        # NOTE: Order the valid combinations to find which one of them makes
        # most available space in a host. This means that the best matching
        # combination is the smallest of the valid ones!
        # e.g. requested = {vcpu: 5, memory: 1024}
        # a = {vcpu: 2, memory: 1024} and b = {vcpu: 5, memory: 1024}
        # from the valid_combos = [a, b] the best_matching would be combo 'a'
        # since it will force the host to make use of the available space
        # TODO(ttsiouts): Reuse scheduler weights for memory and disk
        return min(
            combinations, key=lambda x: (
                x.resources.VCPU,
                x.resources.MEMORY_MB,
                x.resources.DISK_GB
            )
        )
