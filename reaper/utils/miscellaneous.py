import collections
from nova.reaper import conf
from oslo_log import log as logging

LOG = logging.getLogger(__name__)

CONF = conf.CONF

Allocation = collections.namedtuple(
    "Allocation", ("provider", "consumer", "resources"))

Inventory = collections.namedtuple(
    "Inventory", ("example"))

Offer = collections.namedtuple(
    "Offer", ("provider", "consumers", "resources")
)

ServerMapping = collections.namedtuple(
    "ServerMapping", ("servers", "resources")
)


def json_to_allocations(json, consumer):
    allocations = []
    for provider, item in json.items():
        alloc = Allocation(provider,
                           consumer,
                           Resources(item["resources"]))
        allocations.append(alloc)
    return allocations


def flavor_to_resources(flavor):
    """Gets resources from a flavor

    :para flavor: a nova Flavor object to get its resources
    """
    # NOTE: It's better to use the scheduler utils for this
    res = {
        "VCPU": flavor.vcpus,
        "MEMORY_MB": flavor.memory_mb,
        "DISK_GB": flavor.root_gb
    }
    return Resources(res)


def _is_flavor_preemptible(flavor):
    """Checks if the flavor is preemptible

    Checks if the preemptible property is set for the provided flavor.

    :param flavor: the nova Flavor object to check
    """
    # NOTE(ttsiouts): For this stage the preemptible property is set to
    # the flavor. So here we check if the flavor has preemptible=True in
    # its extra_specs
    try:
        return flavor.extra_specs['preemptible']
    except KeyError:
        return False


class Resources(object):
    """Internal representation of resources

    Just a helper class that enables the reaper to make simple calculations
    with resources.
    """
    # NOTE(ttsiouts): Be careful when comparing! Use comparisons for their
    # actual meaning and don't make assumptions
    # Can end up with strange results because the objects are NOT numbers:
    # e.g. a = {vcpu: 3, memory: 1024}, b = {vcpu: 2, memory: 512}
    # a < b = False and a > b = False and a == b = False

    def __init__(self, resources=None):
        """Initialized with a dictionary of resource classes and values"""
        if not resources:
            resources = {
                'VCPU': 0,
                'MEMORY_MB': 0,
                'DISK_GB': 0,
            }
        self.resources = set()
        for resource, value in resources.items():
            setattr(self, resource, value)
            self.resources = self.resources | set([resource])

    def __add__(self, other):
        resources = self.resources | other.resources
        new = dict()
        for res in resources:
            new[res] = getattr(self, res, 0) + getattr(other, res, 0)
        return Resources(new)

    def __sub__(self, other):
        resources = self.resources | other.resources
        new = dict()
        for res in resources:
            value = getattr(self, res, 0) - getattr(other, res, 0)
            new[res] = int(value)
        return Resources(new)

    def __mul__(self, other):
        # NOTE(ttsiouts): Should be used only with numbers... Resource * 3
        new = dict()
        for res in self.resources:
            try:
                new[res] = int(getattr(self, res, 0) * other)
            except TypeError:
                return None
        return Resources(new)

    def __truediv__(self, other):
        # NOTE(ttsiouts): the result of this operation is the minimum result
        # after dividing all the resource classes:
        # e.g. a = {vcpu: 100, memory: 1024}, b = {vcpu: 1, memory: 512}
        #      a / b = 2 (because a.memory / b.memory = 2)
        result = -1
        for resource in other.resources:
            if getattr(self, resource, 0) < getattr(other, resource, 0):
                res = 0
            else:
                # NOTE: Careful with the default values of getattr!
                # division with zero :P
                res = getattr(self, resource, 0) / getattr(other, resource, 1)
            if result == -1 or res < result:
                result = res
        return int(result)

    def __div__(self, other):
        # NOTE(ttsiouts): the result of this operation is the minimum result
        # after dividing all the resource classes:
        # e.g. a = {vcpu: 100, memory: 1024}, b = {vcpu: 1, memory: 512}
        #      a / b = 2 (because a.memory / b.memory = 2)
        result = -1
        for resource in other.resources:
            if getattr(self, resource, 0) < getattr(other, resource, 0):
                res = 0
            else:
                # NOTE: Careful with the default values of getattr!
                # division with zero :P
                res = getattr(self, resource, 0) / getattr(other, resource, 1)
            if result == -1 or res < result:
                result = res
        return int(result)

    def __eq__(self, other):
        resources = self.resources | other.resources
        for res in resources:
            if getattr(self, res, 0) != getattr(other, res, 0):
                return False
        return True

    def __ne__(self, other):
        return not self == other

    def __gt__(self, other):
        resp = True
        for res in self.resources:
            resp = resp and getattr(self, res) > getattr(other, res, 0)
        return resp

    def __lt__(self, other):
        resp = True
        for res in self.resources:
            resp = resp and getattr(self, res) < getattr(other, res, 0)
        return resp

    def __ge__(self, other):
        resp = True
        for res in self.resources:
            resp = resp and getattr(self, res) >= getattr(other, res, 0)
        return resp

    def __le__(self, other):
        resp = True
        for res in self.resources:
            resp = resp and getattr(self, res) <= getattr(other, res, 0)
        return resp

    def __repr__(self):
        text = ', '.join(['%s: %s' % (res, getattr(self, res))
                         for res in self.resources])
        return '<Resources(%s)>' % text

    def to_dict(self):
        tdict = dict()
        for resource in self.resources:
            tdict.update({resource: getattr(self, resource)})
        return tdict
