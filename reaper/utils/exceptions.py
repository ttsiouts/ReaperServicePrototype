from keystoneauth1.exceptions import base as ks_exception
from nova import exception as nova_exception
from oslo_log import log as logging

LOG = logging.getLogger(__name__)


class ReaperException(Exception):
    """Base class for the exceptions thrown by the Reaper"""

    code = 500
    reason = "Unknown error occured"

    def __init__(self, reason=None, **kwargs):
        if reason:
            self.reason = reason
        self._log_exception()

    def _log_exception(self):
        LOG.exception(self.reason)


class NotFound(ReaperException):
    reason = "Resource not found"


class PreemptibleRequest(ReaperException):
    """Thrown when Nova tries to free up space for a preemptible server"""
    reason = "Freeing up space for preemptible servers is not allowed"


def catch_reaper(function):
    """Cathing Reaper exceptions

    In case of failure, with this we catch Reaper exceptions and return to the
    scheduler an empty response.
    """
    def decorator(*args, **kw):
        try:
            return function(*args, **kw)
        except Reaper as e:
            return [], {}
    return decorator

def catch_nova(function):
    """Cathing Nova exceptions

    The purpose is to catch Nova exceptions and map them to their Reaper
    equivalents.
    """
    # TODO(ttsiouts): Define the expected nova exceptions and their reaper
    # equivalents.
    def decorator(*args, **kw):
        try:
            return function(*args, **kw)
        except nova_exception.NovaException as e:
            raise e
    return decorator


def catch_keystone(function):
    """Cathing Keystone exceptions

    The purpose is to catch Keystone exceptions and map them to their Reaper
    equivalents.
    """
    # TODO(ttsiouts): Define the expected keystone exceptions and their reaper
    # equivalents.
    def decorator(*args, **kw):
        try:
            return function(*args, **kw)
        except ks_exception.ClientException:
            raise ReaperException("Generic ")
    return decorator
