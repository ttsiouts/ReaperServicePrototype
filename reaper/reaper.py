from nova import exception as nova_exc
from nova.reaper import conf
from nova.reaper.clients.computes import ComputeClient
from nova.reaper.clients.resources import PlacementClient
from nova.reaper.drivers import chance_driver
from nova.reaper.drivers import strict_driver
from nova.reaper.tags import flavor_based
from nova.reaper.tags import project_based
from nova.reaper.tags import resource_based
from nova.reaper.utils import exceptions
from nova.reaper.utils import miscellaneous as misc

from oslo_log import log as logging
from oslo_serialization import jsonutils

import time

LOG = logging.getLogger(__name__)
CONF = conf.CONF


class Reaper(object):
    """The Reaper Class

    This class decides which preemptible servers have to be terminated.
    """
    def __init__(self, context):
        # TODO(ttsiouts): Load the configured tag and strategy
        #self.tagging = flavor_based.FlavorTagging(context)
        #self.tagging = project_based.ProjectTagging(context)
        self.tagging = resource_based.ResourceTagging(context)
        #self.driver = chance_driver.ChanceDriver(context)
        self.driver = strict_driver.StrictDriver(context)
        self.compute = ComputeClient(context)
        self.placement = PlacementClient()

    def check_request(self, req_spec):
        """Checks the request

        The configured tagging method will check the request_spec. If the
        request is for a preemptible instance, then the procedure is canceled.

        :param req_spec: the request spec to check
        """
        if not self.tagging.validate_request(req_spec):
            raise exceptions.PreemptibleRequest()

    @exceptions.catch_reaper
    def handle_request(self, req_spec, resources):
        """Main functionality of the Reaper

        Gathers info and tries to free up the requested resources.

        :param req_spec: the request specification for the spawning server
        :param resources: the requested resources
        """
        self.check_request(req_spec)
        request = misc.Resources(resources.merged_resources())
        hosts = self.tagging.get_relevant_hosts(request)

        selected_hosts, selected_servers = \
            self.driver.get_preemptible_servers(request, hosts,
                                                req_spec.num_instances)

        response = self.form_scheduler_response(selected_hosts, 
                                                request)

        allocations, provider_summaries = response

        # Culling after forming the response to save time.
        self.cull_preemptible_servers(selected_servers)

        return allocations, provider_summaries

    def free_resources(self, req_spec, resources):
        """Handling of the request

        Triggers the configured driver to free up the requested space. Returns
        the response that will be given to the scheduler.
        """
        return self.driver.free_resources(req_spec, resources)

    def cull_preemptible_servers(self, selected_instances):
        """Culls a preemptible server

        Through the Nova client, places a request to shut off and then delete
        a preemtpible server.
        """
        # NOTE(ttsiouts): Killing and waiting in different loops to save time
        for instance in selected_instances:
            # TODO(ttsiouts): terminate instead of deleting....
            self.compute.delete_instance(instance)

        for instance in selected_instances:
            self.wait_until_allocation_is_deleted(instance)

    def wait_until_allocation_is_deleted(self, instance_id, timeout=10):
        """Wait until the allocation is deleted

        Tries to get the allocations of a given instance until it's not found
        or the timeout exceeds
        """
        # Awful way to wait until the allocation is removed.....
        # Have to live with it for now... If we don't wait here, the claiming
        # of the resources will most probably fail since placement will not be
        # updated right away....
        start = time.time()
        now = start
        while now - start <= timeout:
            try:
                self.placement.get_allocations(instance_id)
            except nova_exc.NotFound:
                break
            now = time.time()

    def form_scheduler_response(self, hosts, request):
        """Form the scheduler response

        This is where the response to the scheduler is being formed
        """
        # TODO(ttsiouts): Add handling for placement microversions.
        # The format of the response is changed...
        if not hosts:
            return [], {}

        allocations = list()
        provider_summaries = dict()

        # NOTE: For each host, form the response to the scheduler
        for host in hosts:
            allocations += host.get_provider_allocations(request)
            provider_summaries.update(host.get_provider_summary(request))

        return jsonutils.loads(jsonutils.dumps(allocations)), \
                   jsonutils.loads(jsonutils.dumps(provider_summaries))
