import itertools
import collections
import random

from nova.reaper.utils import miscellaneous as misc

from oslo_log import log as logging

LOG = logging.getLogger(__name__)


class Host(object):
    """The class containing a host's information"""

    def __init__(self, uuid, host=None, summary=None):
        self.uuid = uuid
        self.preemptible = misc.Resources()
        self.reserved = misc.Resources()
        self.available = misc.Resources()
        self.totals = misc.Resources()
        self.servers = collections.defaultdict()
        self.random_servers = []
        if host:
            self.populate_by_object(host)
        elif summary:
            self.populate_by_summary(summary)

    def populate_by_object(self, host):
        self.set_totals_by_object(host)
        self.set_availables_by_object(host)

    def populate_by_summary(self, summary):
        total_resources = collections.defaultdict()
        used_resources = collections.defaultdict()

        for rc, ammounts in summary.items():
            total_resources[rc] = ammounts['capacity']
            used_resources[rc] = ammounts['used']

        self.totals = misc.Resources(total_resources)
        used = misc.Resources(used_resources)
        self.available = self.totals - used

    def set_totals_by_object(self, host):
        """Sets the total capabilities of the host"""
        # TODO(ttsiouts): Use the request to initilize the relevant resource
        # classes and make use of the following:
        res = {"VCPU": host.vcpus * host.cpu_allocation_ratio,
               "MEMORY_MB": host.memory_mb_used + host.free_ram_mb,
               "DISK_GB": host.local_gb * host.disk_allocation_ratio}
        self.totals = misc.Resources(res)

    def set_availables_by_object(self, host):
        """Sets the initially available resources"""
        # TODO(ttsiouts): Use the request to initilize the relevant resource
        # classes
        res = {"VCPU": self.totals.VCPU - host.vcpus_used,
               "MEMORY_MB": self.totals.MEMORY_MB - host.memory_mb_used,
               "DISK_GB": self.totals.DISK_GB - host.local_gb_used}
        self.available = misc.Resources(res)

    def add_server_allocations(self, servers):
        """Add a preemptible server in the host's mapping.

        :param servers: the server that will be mapped
        """
        for uuid, res in servers.items():
            resources = misc.Resources(res)
            self.preemptible += resources
            self.servers[uuid] = resources

    def add_servers(self, servers):

        for server in servers:
            uuid = server.uuid
            self.servers[uuid] = self.get_server_resources(server)            
            self.preemptible += self.servers[uuid]

    def get_random_offer(self, position):
        """Returns a preemptible servers and its resources

        :param position: the position of the server in the server's list
        """
        if not self.random_servers:
            self.random_servers = self.servers.keys()
            random.shuffle(self.random_servers)

        try:
            server = self.random_servers[position]
            return misc.Offer(self.uuid, [server], self.servers[server])
        except IndexError:
            return misc.Offer(self.uuid, [], misc.Resources())

    def get_server_combinations(self, requested):
        """Returns a list of server combinations

        Here we generate all the preemptible server combinations that the host
        can provide. If the resources of the combination + the amount of free
        resources of the host are enough to free up the requested resources,
        the combination is considered valid.

        :param requested: the requested resources
        """
        combos = list()
        # Even the 0-length combinations are checked, because of scenarios
        # where more than one non-preemptible servers are spawing. E.g.
        # the host has a preemptible server with {vcpu:4, ram:1024, disk:40}.
        # The reaper is triggered to make space for 2 instances with resources
        # {vcpu:2, ram:512, disk:20}. If the preemptible is terminated in the
        # first round of selection, then the host will have enough resources
        # for the second preemptible without having to terminate more servers.
        for num in range(0, len(self.servers.keys()) + 1):

            for servers in itertools.combinations(self.servers.keys(), num):

                # Calculate the resources consumed by each server combination
                resources = misc.Resources()
                for server in servers:
                    resources += self.servers[server]

                # Only the valid server combinations are returned
                if (self.available + resources) >= requested:
                    offer = misc.Offer(self.uuid, list(servers), resources)
                    combos.append(offer)

        return combos

    def validate_request(self, requested):
        """Checks if the request can be handled by this host

        Returns True in case the request can be handled by the host
        """
        virtual = self.available + self.preemptible
        return self.totals >= requested and virtual >= requested

    def get_provider_summary(self, requested):
        """Form the provider summary

        Returns the summary of this provider in the form that scheduler needs
        for the POST to Placement API.
        """
        used = self.totals - (self.available + self.reserved)
        summary = dict()
        for resource in requested.resources:
            summary.update({
                resource: {
                    'used': int(getattr(used, resource, 0)),
                    'capacity': int(getattr(self.totals, resource, 0))
                }
            })
        return {self.uuid: {'resources': summary}}

    def get_provider_allocations(self, requested):
        """Get the allocations from this host

        Returns the allocations in the form that scheduler needs it for the
        POST to Placement API.
        """
        # NOTE: This is not right. When we reserve resources we should create
        # allocations for each resource class and combine them here.
        rdict = requested.to_dict()
        response = {
            'allocations': [{
                'resource_provider': {'uuid': self.uuid},
                'resources': rdict
            }]
        }
        return [response]

    def get_server_resources(self, server):
        """Gets the resources consumed by a server

        For a given instance, this will transform the consumed resources to
        the internal representation (reaper.util.misc.Resources), to enable us
        make computations more efficiently.

        :param server: an instance object from where we extract the usage from
        """
        # This has to be more generic. We might be interested in other types
        # resources too.
        res = {
            "VCPU": server.vcpus,
            "MEMORY_MB": server.memory_mb,
            "DISK_GB": server.root_gb
        }
        return misc.Resources(res)

    def reserve_resources(self, servers, request):
        """Reserving the selected resources

        Reserves the resources that belong to the selected preemptible servers
        in order to be able to do further computations if needed. The point of
        this is to update the host's representation between rounds of server
        selection

        :param servers: the list of selected instances whose resources need to
                        reserved.
        :param request: the requested resources.
        """
        reserved = misc.Resources()
        for server in servers:
            reserved = reserved + self.servers[server]
            self.servers.pop(server, None)
        self.preemptible = self.preemptible - reserved

        # NOTE: If the reserved preemptible resources are less than the
        # requested resources, it means that part of the already available
        # resources need to be reserved too. If more than the requested is
        # freed then we end up with more available space
        remaining = request - reserved
        self.available = self.available - remaining

        # NOTE: Keeping track of the total reserved resources
        self.reserved = self.reserved + reserved + remaining

        # NOTE: Empty the random list to be randomized at the next selection
        if self.random_servers:
            self.random_servers = list()
